resource "aws_security_group" "module_sg_tf" {
 name        = "${terraform.workspace}-module"
 description = "Allow HTTPS to web server"
 #vpc_id      = data.aws_vpc.default.id

ingress {
  cidr_blocks = ["0.0.0.0/0"]
  description = "http ingress"
  protocol    = "tcp"
  from_port = 80
  to_port = 80
}

egress {
   from_port   = 0
   to_port     = 0
   protocol    = "-1"
   cidr_blocks = ["0.0.0.0/0"]
 }
}

